# Première expérience MkDocs

Avant d'utiliser MkDocs, il est conseillé de s'entrainer avec du Markdown avec un éditeur en ligne. Plus d'informations ici : <https://ens-fr.gitlab.io/tuto-markdown/>

!!! faq "Avez-vous les bons outils ?"
    - [ ] un éditeur de code comme VSCodium.
    - [ ] _Material for MkDocs_ installé.

    Alors ?

    - Non ; vous pouvez trouver [les bons outils](prérequis.md).
    - Oui ; vous pouvez continuer.

    Dans la suite on utilisera VSCodium en démonstration, mais
    toute autre combinaison d'outils est possible.

!!! tip "Pas d'ordinateur ?"
    - Vous pouvez faire ce devoir sur tablette en utilisant [Basthon](https://basthon.fr/).
        - Ce site respecte votre vie privée.
    - Vous cliquez sur `Notebook`.
    - Vous travaillez comme avec Jupyter ; ce sera moins joli, c'est tout.
    - Quand votre travail est fini, vous pouvez cliquer sur `Fichier` puis `Enregistrer sous...`
    - Vous déposerez votre fichier `untitled.ipynb` dans le casier électronique du professeur.

## Préparation du projet

1. [Télécharger **ce** projet-ci](https://gitlab.com/ens-fr/experience/-/archive/master/experience-master.zip){ .md-button } ; oui, vous allez modifier le site actuel que vous visitez !!! Oui, **ce** site.
2. Dans votre dossier de téléchargement, vous trouverez `experience-master.zip`.
2. **Extraire** ce fichier `.zip` dans un dossier, où vous voulez. Vous pouvez aussi renommer le dossier.
3. Lancer VSCodium, puis avec ++ctrl+k+o++ ouvrir le dossier `experience-master` (ou le nouveau nom) là où vous l'avez décompressé. Vous devez observer à gauche, par exemple, un fichier `licence` avec des clés, mais **surtout** `mkdocs.yml` et un dossier `docs`.
4. Ouvrir un terminal dans VSCodium 
    - Menus en français : (Affic<u>h</u>age → <u>T</u>erminal) avec ++alt+h+t++.
    - Menus en anglais : (_View_ → _Terminal_) avec ++alt+v+t++.
5. Dans le terminal entrer `mkdocs serve`.
    - Si une erreur survient ici, c'est que vous avez ouvert le mauvais dossier. Recommencez le point 3.
6. Ouvrir votre navigateur à l'adresse indiquée <http://127.0.0.1:8000>
7. Dans VSCodium, dérouler le dossier `docs`
7. Cliquer sur le fichier :material-arrow-down-bold: `index.md`
8. Vous pouvez modifier ce fichier `index.md` ; c'est votre travail !
9. À chaque sauvegarde, votre navigateur se met à jour.

## Vérification

Vous devriez avoir une situation qui ressemble à 
![vscodium](images/vscodium.png)

Si vous avez une erreur, vérifiez que vous êtes bien dans le bon dossier pour votre terminal.

## Consignes

- Vous pouvez modifier le fichier `index.md`.
    - À chaque sauvegarde, votre navigateur sera mis à jour automatiquement.

- Vous pouvez fermer la fenêtre du terminal, et en cas de doute, vous pourrez aller la consulter avec ++alt+h+t++ (ou ++alt+v+t++ avec menus en anglais). Le terminal indique certains avertissements ou des erreurs. Il faut relancer `mkdocs serve` en cas d'erreurs. Pour le stopper, cliquer dessus et entrer ++ctrl+c++.

- Une fois votre page créée (une seule page pour commencer), comment rendre votre travail ?
    - Vous avez seulement construit une seule page `index.md` ? Alors déposer ce fichier dans le casier électronique.
    - Vous avez de grosses modifications ? Alors stopper `mkdocs serve` et lancer ensuite `mkdocs build` pour faire la construction réelle de votre site. Construire un fichier `.zip` avec tout le projet (dossiers `docs` et `site`). Attention, il est interdit d'utiliser WinRAR, votre professeur ne lira pas votre travail. Déposer ce fichier `.zip` dans le casier électronique.

## Les règles

- Vous pouvez faire une copie d'une page d'un site que vous trouvez peu joli, pour le rendre plus joli. Vous devrez alors citer votre source très clairement.
- Vous pouvez reprendre de nombreuses informations sur Wikipédia, toujours en citant les sources.
- Vous pouvez aborder le thème que vous souhaitez tant qu'il est respectueux. Vous ne serez pas noté sur le contenu, mais sur la façon de le mettre en forme et des changements apportés.
- Un document qui parle d'algorithme ou d'informatique sera valorisé, par rapport à un document qui parle de jeux.
- Un document avec des maths est plus difficile à écrire ; danger.
- Faites-vous plaisir !

## Pour les motivés

Une autre version du projet existe avec la possibilité de faire des macros et d'utiliser un IDE Python. :warning: c'est plus technique.

> <https://ens-fr.gitlab.io/exp2/>
