# Prérequis

## Gestionnaire d'archive

=== "Linux"
    Vous avez déjà un outil de décompression d'archive.
    Dans l'explorateur de fichier, on peut faire un clic-droit sur un fichier `.zip`
    et ensuite, dans le menu contextuel, il y a

    - :material-archive-arrow-up-outline: Extraire ici

=== "Mac"
    Sûrement comme Linux... à peu de choses prêt.

=== "Windows"
    On recommande [7zip](https://www.7-zip.fr/) qui est un logiciel _open source_.

    Il vous permet d'avoir un menu contextuel ... ???

## Un éditeur de code

Il y a de nombreuses possibilités.

Pour cette expérience, on recommande [VSCodium](https://lyc-84-bollene.gitlab.io/chambon/%C3%89diteurs/vscodium/).

Cet éditeur est :

- libre, (_open source_)
- sans télémétrie, (contrairement à Visual Studio Code)
- multiplateforme,
- dispose d'un terminal intégré, (très utile),
- et de nombreuses fonctionnalités.

## _Material for MkDocs_

Retrouver les étapes d'installation sur ce [tutoriel](https://ens-fr.gitlab.io/mkdocs/).

## Vérification

Dans un terminal, vous devez obtenir quelque chose qui ressemble à ça :

```console
francky@DUST:~$ mkdocs --version
mkdocs, version 1.1.2 from /home/francky/.local/lib/python3.8/site-packages/mkdocs (Python 3.8)
francky@DUST:~$ codium --version
1.56.2
054a9295330880ed74ceaedda236253b4f39a335
x64
```
